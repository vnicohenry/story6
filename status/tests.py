from django.test import TestCase, Client

from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


from .views import index
from .models import Status
from .forms import StatusForm

class StatusUnitTest (TestCase):
        def test_status_url_exists(self):
                response = Client().get('//')
                self.assertEqual(response.status_code, 200)
		
        def test_model_status(self):
                new_status = Status.objects.create(isi='jangan kepo',date=timezone.now())
                counting = Status.objects.all().count()
                self.assertEqual(counting,1)

        def test_can_save_a_POST_request(self):
                response = self.client.post('//', 
                        data={'isi' : 'Halo!'})
                counting = Status.objects.all().count()
                self.assertEqual(counting, 1)

                
class StatusFunctionalTest(TestCase):
        def setUp(self):
                chrome_options = Options()
                chrome_options.add_argument('--dns-prefetch-disable')
                chrome_options.add_argument('--no-sandbox')        
                chrome_options.add_argument('--headless')
                chrome_options.add_argument('disable-gpu')
                self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
                super(StatusFunctionalTest, self).setUp()

        def tearDown(self):
                self.selenium.quit()
                super(StatusFunctionalTest, self).tearDown()

        def test_input_todo(self):
                selenium = self.selenium
                # Opening the link we want to test
                selenium.get('http://localhost:8000/')
                # find the form element
                form = selenium.find_element_by_id('id_isi')

                submit = selenium.find_element_by_id('postbutton')

                # Fill the form with data
                form.send_keys('baik!')

                # submitting the form
                submit.send_keys(Keys.RETURN)
                self.assertIn('baik!', selenium.page_source)







# Create your tests here.
