import time
from selenium import webdriver
import unittest


class NewVisitorTest(unittest.TestCase):
        def setUp(self):
                self.driver = webdriver.Chrome()

        def test_can_start_a_list_and_retrieve_it_later(self):
                self.driver.get('https://scele.cs.ui.ac.id/')
                time.sleep(5)
                search_box = self.driver.find_element_by_name('search')
                search_box.send_keys('PPW')
                search_box.submit()
                time.sleep(5)
                self.assertIn("Perancangan", self.driver.page_source)

if __name__ == '__main__':
        unittest.main(warnings='ignore')
