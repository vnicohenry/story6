from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def index(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			hasil = Status(isi=form.data['isi'])
			hasil.save()
			return redirect("status:index")
	form = StatusForm()
	hasil = Status.objects.all().order_by('-id')
	return render(request, 'index.html', {'form':form, "hasil":hasil})

# Create your views here.
